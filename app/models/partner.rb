class Partner < ActiveRecord::Base
  attr_accessible :address, :city, :latitude, :longitude, :name
  after_validation :geocode

  geocoded_by :full_address


  def full_address
    "#{address} #{city}"
  end
end
