function locate_user() {

};

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(showPosition);
    }
    else {
        $('.messages').html("Geolocation is not supported by this browser.");
    }
}
;
function showPosition(position) {
    $('.position').html("Latitude: " + position.coords.latitude +
        "<br>Longitude: " + position.coords.longitude);
    getNearest(position);
};

function getNearest(position) {
    $.ajax({
      url: '/finder/nearest',
      data: {latitude: position.coords.latitude, longitude: position.coords.longitude},
      success: function(response) { eval(response.responseText); },
      dataType: 'script'
    });
}
var bearing = 0;
var distance = 0;

$(function () {
    getLocation();
    Compass.watch(function(heading) {
        $('.degrees').text(Math.round(heading));
        $('.distance-display').text(distance);

        $('.compass').css('transform', 'rotate('  + (-heading+bearing) + 'deg)');
    });
});
