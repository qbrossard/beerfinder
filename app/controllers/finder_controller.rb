class FinderController < ApplicationController

  def index

  end

  def nearest
    location = [params[:latitude], params[:longitude]]
    @nearest = Partner.near(location).first
    #@nearest = Partner.near([47.5, 8.5]).first
    @distance = @nearest.distance_from(location)
    @bearing = @nearest.bearing_from(location)
  end
end
