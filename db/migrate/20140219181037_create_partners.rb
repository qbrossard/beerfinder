class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.string :address
      t.string :city
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
